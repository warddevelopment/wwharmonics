# WWHarmonics

[![CI Status](http://img.shields.io/travis/Wills Ward/WWHarmonics.svg?style=flat)](https://travis-ci.org/Wills Ward/WWHarmonics)
[![Version](https://img.shields.io/cocoapods/v/WWHarmonics.svg?style=flat)](http://cocoadocs.org/docsets/WWHarmonics)
[![License](https://img.shields.io/cocoapods/l/WWHarmonics.svg?style=flat)](http://cocoadocs.org/docsets/WWHarmonics)
[![Platform](https://img.shields.io/cocoapods/p/WWHarmonics.svg?style=flat)](http://cocoadocs.org/docsets/WWHarmonics)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WWHarmonics is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "WWHarmonics"

## Author

Wills Ward, wward@willworks.net

## License

WWHarmonics is available under the MIT license. See the LICENSE file for more info.

